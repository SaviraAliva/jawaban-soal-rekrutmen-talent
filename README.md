# Jawaban Soal Rekrutmen Talent

Jawaban soal dari Rekrutmen Arkademy SMK Telkom Malang
Savira Vilza Aliva XIIRPL1

Pengujian soal nomor 1 dengan PHP bisa di uji di [Jawaban Nomor 1](https://ideone.com/dlNbGu)

Untuk Soal Logika Nomor 3, 4, dan 5 menggunakan Java
Bisa dilihat hasil compilenya melalui link berikut :

*   [Nomor 3](https://ideone.com/vDyYiX)
*   [Nomor 4](https://ideone.com/PoaV1Z)
*   [Nomor 5](https://ideone.com/8I6pmE)

Untuk mengujinya dapat mengeklik **fork** di kiri atas kode bar